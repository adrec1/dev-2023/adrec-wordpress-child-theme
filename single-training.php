<?php
/**
 * The template for displaying all single posts
 *
 * @package Understrap
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;

get_header();
$container = get_theme_mod('understrap_container_type');
?>

    <div class="wrapper" id="single-wrapper">

        <div class="<?php echo esc_attr($container); ?>" id="content" tabindex="-1">

            <div class="row">
                <main class="site-main" id="main">

                    <?php
                    while (have_posts()) {
                        the_post();

// Exit if accessed directly.
                        defined('ABSPATH') || exit;
                        ?>

                        <article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

                            <header class="entry-header">

                                <?php the_title('<h1 class="entry-title">', '</h1>'); ?>

                                <div class="entry-meta">

                                    Posté le <?php echo get_the_date('d/m/Y'); ?>

                                </div><!-- .entry-meta -->

                                <div>
                                    <ul>
                                        <?php if (!empty(get_field('duration'))): ?>
                                            <li>
                                                <strong>Durée de la formation
                                                    : </strong> <?php echo get_field('duration'); ?>
                                            </li>
                                        <?php endif; ?>

                                        <?php if (!empty(get_field('public'))): ?>
                                            <li>
                                                <strong>Public visé
                                                    : </strong> <?php echo get_field('public')['label']; ?>
                                            </li>
                                        <?php endif; ?>

                                        <?php if (!empty(get_field('pmr_access'))): ?>
                                            <li>
                                                <strong>Accès PMR : </strong>
                                                <?php if (get_field('pmr_access')): ?>
                                                    Oui
                                                <?php else: ?>
                                                    Non
                                                <?php endif; ?>
                                            </li>
                                        <?php endif; ?>

                                    </ul>
                                </div>

                            </header><!-- .entry-header -->

                            <?php echo get_the_post_thumbnail($post->ID, 'large'); ?>

                            <div class="entry-content">

                                <?php
                                the_content();
                                understrap_link_pages();
                                ?>

                            </div><!-- .entry-content -->


                        </article>

                        <?php
                    }
                    ?>

                </main>

                <div class="container">
                    <div class="row justify-content-center mt-4">
                        <?php if (!empty(get_field('teachers'))): ?>
                            <?php foreach (get_field('teachers') as $teacher): ?>
                                <div class="col-md-2">
                                    <div class="teacher-card">
                                        <img src="<?php echo get_the_post_thumbnail_url($teacher->ID, 'medium') ?>" alt="">
                                        <?php echo $teacher->post_title; ?>
                                    </div>

                                </div>

                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>


            </div><!-- .row -->

        </div><!-- #content -->

    </div><!-- #single-wrapper -->

<?php
get_footer();

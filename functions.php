<?php
require_once 'vendor/autoload.php';
function understrapChildEnqueueScripts() {
    wp_enqueue_style( 'understrap-child-main-style', get_stylesheet_directory_uri() . '/assets/css/main.css', ['understrap-styles'], 'v1.0.1' );
}
add_action( 'wp_enqueue_scripts', 'understrapChildEnqueueScripts' );

remove_filter('get_the_excerpt', 'wp_trim_excerpt');

require 'core/post-type/training.php';
require 'core/post-type/teacher.php';

// remove version from head
remove_action('wp_head', 'wp_generator');

// remove version from rss
add_filter('the_generator', '__return_empty_string');

// remove version from scripts and styles
function shapeSpace_remove_version_scripts_styles($src) {
    if (strpos($src, 'ver=')) {
        $src = remove_query_arg('ver', $src);
    }
    return $src;
}
add_filter('style_loader_src', 'shapeSpace_remove_version_scripts_styles', 9999);
add_filter('script_loader_src', 'shapeSpace_remove_version_scripts_styles', 9999);
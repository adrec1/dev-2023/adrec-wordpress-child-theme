<div class="card default-card">

    <?php
        if(get_the_post_thumbnail_url(get_the_ID(), 'medium')) {
            $imgUrl = get_the_post_thumbnail_url(get_the_ID(), 'medium');
        } else {
            $imgUrl = get_stylesheet_directory_uri() . '/assets/img/default_post.jpg';
        }
    ?>

    <img src="<?php echo $imgUrl; ?>" class="card-img-top"
         alt="<?php get_post_meta(get_post_thumbnail_id( get_the_ID() ), '_wp_attachment_image_alt', true); ?>">
    <div class="card-body">
        <h5 class="card-title">
            <?php echo get_the_title();?>
        </h5>
        <div class="card-text">
            <?php echo get_the_excerpt(); ?>
        </div>
        <a href="<?php echo get_post_permalink(); ?>" class="btn btn-primary">Lire la suite</a>
    </div>
</div>
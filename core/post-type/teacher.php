<?php
function teacherRegister() {

    $supports = array(
        'title', // post title
        'thumbnail', // featured images
        'custom-fields',
    );

    $labels = array(
        'name' => _x('Formateurs', 'plural'),
        'singular_name' => _x('Formateur', 'singular'),
        'menu_name' => _x('Formateur', 'admin menu'),
        'name_admin_bar' => _x('Formateur', 'admin bar'),
        'add_new' => _x('Ajouter un Formateur', 'add new'),
        'add_new_item' => __('Ajouter un Formateur'),
        'new_item' => __('Ajouter un Formateur'),
        'edit_item' => __('Modifier un Formateur'),
        'view_item' => __('Voir le Formateur'),
        'all_items' => __('Voir tous les Formateurs'),
        'search_items' => __('Rechercher un Formateur'),
        'not_found' => __('Aucun Formateur trouvé'),
    );

    $args = array(
        'supports' => $supports,
        'labels' => $labels,
        'public' => true,
        'query_var' => true,
        'has_archive' => false,
        'hierarchical' => false,
        'menu_icon' => 'dashicons-welcome-learn-more',
       
    );
    register_post_type('teacher', $args);
}
add_action('init', 'teacherRegister');

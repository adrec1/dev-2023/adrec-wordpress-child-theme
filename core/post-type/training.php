<?php
function trainingRegister() {

    $supports = array(
        'title', // post title
        'editor', // post content
        'thumbnail', // featured images
    );

    $labels = array(
        'name' => _x('Formations', 'plural'),
        'singular_name' => _x('Formation', 'singular'),
        'menu_name' => _x('Formations', 'admin menu'),
        'name_admin_bar' => _x('formation', 'admin bar'),
        'add_new' => _x('Ajouter une formation', 'add new'),
        'add_new_item' => __('Ajouter une formation'),
        'new_item' => __('Ajouter une formation'),
        'edit_item' => __('Modifier une formation'),
        'view_item' => __('Voir la formation'),
        'all_items' => __('Voir toutes les formations'),
        'search_items' => __('Rechercher une formation'),
        'not_found' => __('Aucune formation trouvée'),
    );

    $args = array(
        'supports' => $supports,
        'labels' => $labels,
        'public' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'formations'), // Si modifié -> refaire permaliens
        'has_archive' => true,
        'hierarchical' => false,
        'menu_icon' => 'dashicons-welcome-learn-more',
        'show_in_rest' => true,
        'taxonomies' => ['training_category'],
    );
    register_post_type('training', $args);


    register_taxonomy(
        'training_category',
        'training',
        [
            'label' => __('Thématique de formation'),
            'rewrite' => ['slug' => 'thematique-de-formation'],
            'hierarchical' => true,
        ]
    );

}
add_action('init', 'trainingRegister');

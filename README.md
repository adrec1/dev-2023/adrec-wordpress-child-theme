# Adrec WordPress custom thèmes

## Installation
- Open a terminal at / of the theme

- Make sure you have sass on your PC. To install :
```
$  npm install -g sass
```
- Compile assets with the next command

```
$  sass --watch .\assets\scss\index.scss .\assets\css\main.css 
```

- Install composer dependencies

```
$  composer install
```
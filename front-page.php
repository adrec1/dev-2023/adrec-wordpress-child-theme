<?php
// Exit if accessed directly.
defined('ABSPATH') || exit;

$lastPostsQuery = new WP_Query([
    'post_type' => 'post',
    'posts_per_page' => 4,
]);

get_header();
$container = get_theme_mod('understrap_container_type');

if (is_front_page()) {
    get_template_part('global-templates/hero');
}

$wrapper_id = 'full-width-page-wrapper';
if (is_page_template('page-templates/no-title.php')) {
    $wrapper_id = 'no-title-page-wrapper';
}

?>
    <div class="container">
        <div class="row">
            <div class="col-12 text-center mt-4">
                <a class="btn btn-primary" href="<?php echo get_post_type_archive_link('training');?>">
                    Voir toutes les formations
                </a>
            </div>
        </div>
    </div>
    <div class="wrapper"
         id="<?php echo $wrapper_id; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped -- ok. ?>">
        <div class="container">
            <div class="row">
                <div class="col-6">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aliquam asperiores commodi
                    consequuntur corporis culpa dolorem dolores, eligendi, ex ipsa iure magnam, obcaecati odio officia
                    tempora ullam veniam voluptatem voluptates?
                </div>
                <div class="col-6">
                    <img src="https://production-laprovence.twic.pics/media/2015/11/03/kaamelott.jpg?twic=v1/focus=0x0/cover=1140x641"
                         alt="">
                </div>
            </div>
        </div>
        <div class="<?php echo esc_attr($container); ?>" id="content">
            <div class="row">
                <div class="col-md-12 content-area" id="primary">
                    <main class="site-main" id="main" role="main">
                        <?php the_content(); ?>
                    </main>
                </div><!-- #primary -->
            </div><!-- .row -->
        </div><!-- #content -->
        <section class="last-posts">
            <div class="container">
                <div class="row">

                    <?php if ($lastPostsQuery->have_posts()) : ?>

                        <!-- pagination here -->

                        <!-- the loop -->
                        <?php while ($lastPostsQuery->have_posts()) : $lastPostsQuery->the_post(); ?>
                            <div class="col-sm-6 col-lg-3 mb-4">
                               <?php get_template_part('partials/card/card', 'default'); ?>
                            </div>
                        <?php endwhile; ?>


                        <!-- end of the loop -->
                        <!-- pagination here -->

                        <?php wp_reset_postdata(); ?>

                    <?php else : ?>
                        <p><?php _e("Sorry j'ai rien fait"); ?></p>
                    <?php endif; ?>

                </div>
            </div>
        </section>


    </div><!-- #<?php echo $wrapper_id; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped -- ok. ?> -->

<?php
get_footer();
